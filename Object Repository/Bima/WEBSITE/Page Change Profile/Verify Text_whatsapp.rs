<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verify Text_whatsapp</name>
   <tag></tag>
   <elementGuidId>03b55ea8-1ef2-4191-81b9-a17803e74009</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div[3]/div/strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>d7f68123-a1c9-4745-ac34-21b50a94be0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The whatsapp must be between 10 and 12 digits.</value>
      <webElementGuid>83ae0d2a-8e2f-42a5-a153-216e8e2be37b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-4&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;invalid-feedback&quot;]/strong[1]</value>
      <webElementGuid>fff38141-4e04-4062-b3f9-485ad3e00e8c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div[3]/div/strong</value>
      <webElementGuid>dd572a06-062d-4199-b6ff-fcfb52b17286</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone'])[1]/following::strong[1]</value>
      <webElementGuid>298a0e44-d04c-484a-a4da-e7e2871daa15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::strong[1]</value>
      <webElementGuid>a5d43124-c3bb-45db-b38e-1181c5b5b73d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BirthDay'])[1]/preceding::strong[1]</value>
      <webElementGuid>6c1a2e83-89e7-4e49-8682-abe90406e4bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::strong[1]</value>
      <webElementGuid>4b392031-a3ab-43e3-9aa2-c4817f6d6d76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The whatsapp must be between 10 and 12 digits.']/parent::*</value>
      <webElementGuid>5fc57049-e8f5-49ea-bae5-7ea33b884d5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>ef073fad-e255-49ee-a33a-950903b6e72b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'The whatsapp must be between 10 and 12 digits.' or . = 'The whatsapp must be between 10 and 12 digits.')]</value>
      <webElementGuid>7df53f67-964e-475c-bff0-5bc49c76ea21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
