<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Fullname</name>
   <tag></tag>
   <elementGuidId>9c3ebcf7-dc77-4eb8-9449-22b548b5c619</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.author-box > div.row.justify-content-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>39e4253f-e1f9-4f0e-82b1-92cf6de017b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row justify-content-center</value>
      <webElementGuid>09e3c945-47a8-423b-be01-0de8716025da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                </value>
      <webElementGuid>c7fbb2b1-a503-4543-aa2d-9eea09233b4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]</value>
      <webElementGuid>4e35edf5-d842-46b3-9010-fbaf27a8346a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div</value>
      <webElementGuid>cb8a463d-0ba7-4bb6-a1bd-c42b4acc8f24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[6]</value>
      <webElementGuid>6e6a6de5-416a-40be-b15a-536c6fda44e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/div/div/div</value>
      <webElementGuid>79553c47-c897-45d3-b8aa-59b59ea24248</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                ' or . = '
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                ')]</value>
      <webElementGuid>39510475-a6e8-4e32-8532-2d5d56a9bd85</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
