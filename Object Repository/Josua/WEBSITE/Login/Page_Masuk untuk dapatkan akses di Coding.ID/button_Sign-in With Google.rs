<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign-in With Google</name>
   <tag></tag>
   <elementGuidId>3f3375da-3b26-4b03-9f6e-4c470a7fe604</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonGoogleTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonGoogleTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>bb84f77d-a168-4230-8491-b854b5f2c777</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonGoogleTrack</value>
      <webElementGuid>d4144520-6cc1-4d0f-8d58-b68d4946e356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0e5e281e-33c3-40c4-88e1-aa9a7efd03d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn wm-all-events btn-block bg-white</value>
      <webElementGuid>90a27ddd-2245-4c3f-84fd-c769ff67d25a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            Sign-in With Google
                                                        </value>
      <webElementGuid>8cb45287-d3f1-44ad-a6a6-ab339024c20b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonGoogleTrack&quot;)</value>
      <webElementGuid>37cba60e-ed4d-4e88-ad08-15a195516bf6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonGoogleTrack']</value>
      <webElementGuid>c158f8cf-59cc-4a53-a11a-34a38bcfa4e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::button[2]</value>
      <webElementGuid>0f5b4c1d-6b13-481f-9216-c0622b854a5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[1]</value>
      <webElementGuid>fbabd98d-7ba3-468c-a551-8532711397df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign-in With Google']/parent::*</value>
      <webElementGuid>e98f104c-605b-45d5-a4b5-007055ccb50a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a/button</value>
      <webElementGuid>46fe4831-61d2-45aa-a1ae-dd949b0f7d1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonGoogleTrack' and @type = 'button' and (text() = '
                                                            
                                                            Sign-in With Google
                                                        ' or . = '
                                                            
                                                            Sign-in With Google
                                                        ')]</value>
      <webElementGuid>93cca47a-651a-49b9-a59d-e3d4b33ff30b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
