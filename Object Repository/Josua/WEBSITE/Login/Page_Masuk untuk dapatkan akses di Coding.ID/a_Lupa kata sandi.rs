<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lupa kata sandi</name>
   <tag></tag>
   <elementGuidId>c5f0596d-6692-46a6-bdbb-d395c4e472ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='buttonForgetPassTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonForgetPassTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>cd8654f3-3a3b-4bce-b2b0-15fd8b7c8637</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonForgetPassTrack</value>
      <webElementGuid>1e5d97f6-6680-4bd9-8cf8-4f3a567b4968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://coding.id/password/reset</value>
      <webElementGuid>a0294cfe-1f78-4ab7-8c3d-ff0c8bcf7449</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lupa kata sandi ?</value>
      <webElementGuid>abccb0f5-881d-4540-8edc-9920a3cc0364</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonForgetPassTrack&quot;)</value>
      <webElementGuid>53493a66-8e8e-4308-a8de-9b04b68fd2e2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='buttonForgetPassTrack']</value>
      <webElementGuid>6f092a0d-3d4c-47c6-9eea-bd9b28027974</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lupa kata sandi ?')]</value>
      <webElementGuid>4fbf90eb-3a2d-4250-82ac-d96d87d98e38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat akun'])[1]/preceding::a[1]</value>
      <webElementGuid>d740e5cc-fb3c-4be6-9d07-9ae94c63118d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lupa kata sandi ?']/parent::*</value>
      <webElementGuid>cf224eeb-2b50-490d-ac5e-b090047cc119</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://coding.id/password/reset')]</value>
      <webElementGuid>b5322c23-1613-42f2-8678-0892d558d865</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div/div/a</value>
      <webElementGuid>b702288a-ed1a-48b6-917e-dcb91af15ec8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'buttonForgetPassTrack' and @href = 'https://coding.id/password/reset' and (text() = 'Lupa kata sandi ?' or . = 'Lupa kata sandi ?')]</value>
      <webElementGuid>81c5ebd2-6f1d-47a1-b0bf-6372742057b1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
