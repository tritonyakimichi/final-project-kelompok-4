<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Whatsapp_whatsapp</name>
   <tag></tag>
   <elementGuidId>9cffda7a-530d-4724-a12a-b3afe7fa973b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='whatsapp']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#whatsapp</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d9d63246-1a6a-49a8-bfe0-a26e8a1d0375</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>87f23a95-c717-4b3b-93b2-63883c1f4ee2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>number</value>
      <webElementGuid>ee0400eb-d059-427f-afd0-9fd5f109783f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>WhatsApp </value>
      <webElementGuid>9614b041-c337-47c0-8d72-0e191c90bd00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>cee676a7-b5a9-423a-ba26-59486184b1f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>67ae856c-e49e-42aa-b281-f45da264b898</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>ce92c969-7e10-4d49-8ea4-55e58e4def0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;whatsapp&quot;)</value>
      <webElementGuid>0fa3174b-79eb-4c50-bd5c-71484660cd82</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='whatsapp']</value>
      <webElementGuid>c97423ce-8f63-4c79-bcb2-c96f77874b98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/input</value>
      <webElementGuid>c54404a9-3605-4902-b0ea-9cc01afd1241</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'whatsapp' and @type = 'number' and @placeholder = 'WhatsApp ' and @name = 'whatsapp']</value>
      <webElementGuid>c5d61c4c-c324-48f1-a817-664d487500d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
