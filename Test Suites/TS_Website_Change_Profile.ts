<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Website_Change_Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>518f36a5-6de8-4597-84ab-e9315b021115</testSuiteGuid>
   <testCaseLink>
      <guid>5fab1699-ade3-4f94-900b-1b29639d7030</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Positive/TCWC002 - That user change Name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c356f785-41ce-4433-866f-cdb744acca7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Positive/TCWC003 - That user change Whatsapp number with correct details</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d66203b2-64c0-474e-9f36-cad4c3cc6024</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Positive/TCWC004 - That user change Birthdate with correct details</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2b23be06-d7a9-4dc2-a705-d9f2e32ad3eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC006 - That user change email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>65a2e0fc-f35b-4d47-b1a6-b632a9121b23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC007 - That user change Whatsapp number below 10 character</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5bfa8787-5e94-4651-a1e2-9512dbeed434</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC008 - That user change Whatsapp number above 12 character</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ab0192bc-a2f5-49f3-b6c4-ab8674eedb2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC010 - That user change Name more than 30 character</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>58772b93-9eda-4cac-95be-527ec20ed04f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC011 - That user didn t fill in the fullname field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a3c8a563-2553-4a68-b8ae-a0d12c919d10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC012 - That user change fullname with alphabet and special character</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d824ba20-e97d-42d8-a05d-ef7ce7f42596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC013 - That user change fullname with alphabet and number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0bce04db-ac1a-4474-885b-c294cb0c3ba2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC014 - That user didn t fill in the Phone field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d10a0b7a-52a6-4ec1-b607-519cb2206738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC015 - That user change Whatsapp with number and alphabet</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a92a4d15-4fd5-40a2-8aad-85ef6bcc498c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC016 - That user change Phone number with number and special character</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>78f6af7e-4ba5-4c3d-8544-2d18ebc1d355</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Positive/TCWC017 - That user cancel Change profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5066032e-dcb2-4388-a1fc-99f4caa889c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC018 - That user change fullname with special character only</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>702b5410-b3de-4299-824f-35150e7a96cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Negative/TCWC019 - That user change fullname with number only</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5726da23-2aa3-40f1-84e2-8ca569c36169</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW003 - Change Profile/Positive/TCWC020 - That user save changes without change anything</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
