<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Website_Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>06f5c06d-0dbe-486f-a648-092a766b8ec5</testSuiteGuid>
   <testCaseLink>
      <guid>2d3596d9-fd05-437e-b7c4-c5524da0a37b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Positive/TCWL001-Login with Correct Email and Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4b696887-a0eb-4081-930b-8da8a43dc70d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL002-Login with Invalid Email and Correct Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>46df9334-cdb4-4a6a-aece-37c3c5acac16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL003-Login with correct email and invalid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>24016176-0f4f-4d73-957c-01cd7df5a360</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL004-Login with incorrect email and incorrect password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4a006a90-f180-497e-8fea-e3080aff81ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL005-Login with the correct email and unfilled password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e3450894-6841-4086-8327-1b8c96fe3fbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL006-Login with the incorrect email and unfilled password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2170f49e-4900-4f28-b9fd-23520e21c050</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL007-Login with unfilled email and correct password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>27239372-2652-4de2-b32a-94e4da5e30bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL008-Login with the unfilled email and incorrect password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d311f995-8eb8-45d7-be4a-15df2900442f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL009-Login with the unfilled email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>81a8eba7-14be-4907-9761-1a8c40cb5f81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Positive/TCWL010-Click Lupa kata sandi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>51fb9b65-c2fb-4167-b6c8-1d8994de0420</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Positive/TCWL011-Click Sign-in with google</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a5e97692-9b7d-42c6-ae67-1c5bbb9e935b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL003-Login with correct email and invalid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0596f374-ae43-4759-96b4-992f6be3cce5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/SCW001 - Login/Negative/TCWL002-Login with Invalid Email and Correct Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
