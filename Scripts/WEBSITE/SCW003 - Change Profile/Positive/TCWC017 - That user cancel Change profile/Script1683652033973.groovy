import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/login')

WebUI.setText(findTestObject('Object Repository/Bima/WEBSITE/Page login/input email'), 'tritonyhp@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Bima/WEBSITE/Page login/input password'), '8SQVv/p9jVTHLrggi8kCzw==')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Bima/WEBSITE/Page login/btn Login'))

WebUI.click(findTestObject('Bima/WEBSITE/Homepage/logo profile'))

WebUI.verifyElementText(findTestObject('Bima/WEBSITE/Homepage/text My Account'), 'My Account')

WebUI.click(findTestObject('Bima/WEBSITE/Homepage/text My Account'))

WebUI.click(findTestObject('Object Repository/Bima/WEBSITE/Page Change Profile/Menu Profile'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Bima/WEBSITE/Page Change Profile/btn_text Edit Profile'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Bima/WEBSITE/Page Change Profile/button_Cancel'))

WebUI.takeScreenshot()

WebUI.verifyElementText(findTestObject('Bima/WEBSITE/Page Change Profile/Verify_Detail Information'), 'Detail Information')

WebUI.closeBrowser()

