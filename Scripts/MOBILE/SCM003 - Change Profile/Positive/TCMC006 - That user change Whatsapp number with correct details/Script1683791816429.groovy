import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Bima\\AppData\\Roaming\\npm\\node_modules\\appium\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - Login Here'), 0)

Mobile.setText(findTestObject('Object Repository/Bima/MOBILE/Field - Email'), 'tritonyhp@gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Bima/MOBILE/Field - Password'), 'password123', 0)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - Login'), 0)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - Profile'), 0)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - Setting'), 0)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - Edit Profile'), 0)

Mobile.setText(findTestObject('Bima/MOBILE/Field - Phone'), '1234567890', 0)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - Save Changes'), 0)

Mobile.tap(findTestObject('Object Repository/Bima/MOBILE/Button - OKAY'), 0)

Mobile.closeApplication()

